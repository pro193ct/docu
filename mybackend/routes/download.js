const express = require('express')
const router = express.Router()
const fs = require('fs');
const Document = require('../models/Document')

const download = async function (req, res, next) {
    const id = req.params.id
    const document = await Document.findById(id)
    // res.json(document.path)

    // var file = fs.createReadStream(document.path);
    // var stat = fs.statSync(document.path);
    // res.setHeader('Content-Length', stat.size);
    // res.setHeader('Content-Type', 'application/pdf');
    // res.setHeader('Content-Disposition', 'attachment; filename=quote.pdf');
    // file.pipe(res);
    ///////////////////////////////////////////////////////////////////////////////////////
    // var data =fs.readFileSync(document.path);
    // res.contentType("application/pdf");
    // res.send(data);

    res.download(document.path); 
}

router.post('/:id', download)
module.exports = router