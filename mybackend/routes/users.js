const express = require('express')
const { reset } = require('nodemon')
const router = express.Router()
const SendingDetail = require('../models/SendingDetail')
const Document = require('../models/Document')
const User = require('../models/User')

const user = [
  // {
  //   id:'001',
  //   email: 'Arabic@go.buu.ac.th',
  //   name:'Arabic Cobra ',
  //   type:'teacher',
  //   branch: 'IT',
  //   studentId:'none',
  // },
  // {
  //   id:'002',
  //   email: 'Brabic@go.buu.ac.th',
  //   name:'Brabic Dragon',
  //   type:'teacher',
  //   branch: 'SE',
  //   studentId:'none',
  // },
  // {
  //   id:'003',
  //   email: 'Dreck@go.buu.ac.th',
  //   name:'Dreck Koona', 
  //   type:'teachers', 
  //   branch: 'CS',
  //   studentId:'none',
  // },
  // { 
  //   id: '004',
  //   email: 'Kanom@go.buu.ac.th',
  //   name: 'Kanom Room',
  //   type:'officer',
  //   branch: 'CS',
  //   studentId:'none',
  // },
  // {
  //   id: '005',
  //   email: '61160251@go.buu.ac.th',
  //   name: 'Tanapat Sakultalakul',
  //   type:'student',
  //   branch: 'CS',
  //   studentId:'61160251',
  // }
]

const getUsers = async function (req,res,next){
  try{
    const users = await User.find({}).then()
    res.json(users)
  } catch(err){
    return res.status(500).send({
      message: err.message
    })
  }
}

const getUser = async function(req,res,next){
  const id = req.params.id
    try{
        const user = await User.findById(id).exec()
        res.json(user)
        if (user==null){
            return res.status(404).json({
                message: 'user not found'
            })
        }
        // res.json(users)
    }catch (err){
      return res.status(404).json({
            message: err.message
        })
    }
  
}
const addUsers = async function (req,res,next){
  const newUser = new User({
    email: req.body.email,
    password: req.body.password,
    roles: req.body.roles,
    name: req.body.name,
    type: req.body.type,
    branch: req.body.branch,
    studentId: req.body.studentId,
    teacherId:  req.body.teacherId,
  })
  try { 
      await newUser.save()
      res.status(201).json(newUser)  
  }catch (err){  
      return res.status(201).send({
        message: err.message
      })
  }
}
const updateUser = async function (req,res,next){
  const userId = req.params.id
    try{
        const user = await User.findById(userId)
        user.email = req.body.email,
        user.password = req.body.password,
        user.name = req.body.name,
        user.type = req.body.type,
        user.branch = req.body.branch,
        user.studentId = req.body.studentId,
        await user.save()
        return res.status(200).json(user)
    }catch (err){
        return res.status(404).send({message: err.message})
    }
}
const deleteUser = async function (req,res,next){
  const userId = req.params.id

  const document = await Document.find({ userId: userId }).exec()
  const sendingDetailId = await SendingDetail.find({ documentId: document._id }).exec()
  
  
  try{
      if(sendingDetailId.length != 0){
        await SendingDetail.findByIdAndDelete(sendingDetailId)
      }
      if(document.length != 0){
        await fs.unlinkSync(document.path)
        await Document.findByIdAndDelete(document)
      }
      
      await User.findByIdAndDelete(userId)
      return res.status(200).send()
  }catch (err){
      return res.status(404).send({message: err.message})
  }
}

router.get('/', getUsers)
router.get('/:id', getUser)
router.post('/', addUsers)
router.put('/:id', updateUser)
router.delete('/:id', deleteUser)

module.exports = router
