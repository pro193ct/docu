const express = require('express')
const { reset } = require('nodemon')
const router = express.Router()
const SendingDetail = require('../models/SendingDetail')
const Document = require('../models/Document')
const User = require('../models/User')
const sendingEmail = require('../routes/sendingEmail');


const sendingDetails = [
    // {
    //     id:'001',
    //     doc_id:'001',
    //     stu_id:'005',
    //     tec_id:'002',
    //     note:'can you sign this file pls'
    // },
    // {
    //     id:'002',
    //     doc_id:'002',
    //     stu_id:'005',
    //     tec_id:'003',
    //     note:'can you sign this file pls'
    // }
]
const getSendingDetails = async function (req, res, next) {
    try {
        const sendingDetails = await SendingDetail.find({}).sort({updatedAt: 'desc'}).then()
        res.json(sendingDetails)
    } catch (err) {
        return res.status(500).send({
            message: err.message
        })
    }
}

const getSendingDetail = async function (req, res, next) {
    const id = req.params.id
    try {
        const sendingDetail = await SendingDetail.find({ $or: [{ studentId: id }, { teacherId: id }] }).sort({updatedAt: 'desc'}).exec()
        var sendingList = []
        for (i = 0; i <sendingDetail.length ; i++) {
            var detail = {}
            const document = await Document.findOne({ _id: sendingDetail[i].documentId })
            const student = await User.findOne({ _id: sendingDetail[i].studentId })
            // var month = sendingDetail[i].createdAt.getUTCMonth() + 1; //months from 1-12
            // var day = sendingDetail[i].createdAt.getUTCDate();
            // var year = sendingDetail[i].createdAt.getUTCFullYear();
            // const newdate = day + "/" + month + "/" + year;
            detail.studentId = student.studentId
            detail.documentId = sendingDetail[i].documentId
            detail.path = document.path
            detail.fileName = document.fileName


            const date = new Date(document.updatedAt)

            detail.date1 = date.toLocaleDateString('th-TH', {
                year: 'numeric',
                month: 'long',
                day: 'numeric',
                // weekday: 'long',
            })

            // detail.date1 = document.updatedAt

            sendingList.push(detail)
            console.log(sendingDetail)
        }
        res.json(sendingList)
        if (sendingList == null) {
            return res.status(404).json({
                message: 'SendingDetail not found'
            })
        }
    } catch (err) {
        res.status(404).json({
            message: err.message
        })
    }
}
const addSendingDetails = async function (req, res, next) {
    const document = await Document.findOne({ _id: req.body.documentId })
    const user = await User.findOne({ _id: req.body.teacherId })
    const newSendingDetail = new SendingDetail({
        documentId: req.body.documentId,
        studentId: req.body.studentId,
        teacherId: req.body.teacherId,
    })
    try {
        var detail = {}
        detail.email = user.email
        detail.fileName = document.fileName
        detail.path = document.path
        detail.note = "กรุณาเซ็นเอกสารด้วย"
        document.status = "กำลังดำเนินการ"
        await document.save()
        await newSendingDetail.save()
        sendingEmail.sendingMail(detail)
        res.status(201).json(newSendingDetail)
    } catch (err) {
        return res.status(201).send({
            message: err.message
        })
    }
}

const updateSendingDetail = async function (req, res, next) {
    const sendingDetailId = req.params.id
    try {
        const sendingDetail = await SendingDetail.findById(sendingDetailId)
        sendingDetail.documentId = req.body.documentId,
            sendingDetail.studentId = req.body.studentId,
            sendingDetail.teacherId = req.body.teacherId,
            await sendingDetail.save(sendingDetail)
        return res.status(200).json(sendingDetail)
    } catch (err) {
        return res.status(404).send({ message: err.message })
    }
}

const deleteSendingDetail = async function (req, res, next) {
    const sendingDetailId = req.params.id
    try {
        // const sendingDetail = await SendingDetail.findById(sendingDetailId)
        const document = await Document.findById(sendingDetailId)
        document.status = "เอกสารถูกยกเลิก"
        await document.save()
        // await SendingDetail.findByIdAndDelete(sendingDetailId)
        await SendingDetail.findOneAndDelete({ documentId: sendingDetailId })
        return res.status(200).send()
    } catch (err) {
        return res.status(404).send({ message: err.message })
    }
}

router.get('/', getSendingDetails)
router.get('/:id', getSendingDetail)
router.post('/', addSendingDetails)
router.put('/:id', updateSendingDetail)
router.delete('/:id', deleteSendingDetail)


module.exports = router