const express = require('express')
const { reset } = require('nodemon')
const router = express.Router()
const User = require('../models/User')


const getSearch = async function (req,res,next){
    try{
        const users = await User.find({type:'อาจารย์'}).then()
        res.json(users)
      } catch(err){
        return res.status(500).send({
          message: err.message
        })
      }
}


router.get('/', getSearch)

  
module.exports = router