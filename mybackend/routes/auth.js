const express = require('express')
const { reset } = require('nodemon')
const router = express.Router()
const bcrypt = require('bcryptjs')
const User = require('../models/User')
const { generateAccessToken } = require('../helpers/auth')


const login = async function(req,res,next){
    const email = req.body.email
    const password = req.body.password

      try{
          const user = await User.findOne({ email: email}).exec()
          const verifyResult = await bcrypt.compare(password, user.password)

          if (!verifyResult){
              return res.status(404).json({
                  message: 'user not found'
              })
          }

          const token = generateAccessToken({_id:user._id , email: user.email})
          res.json({ user: user, email : user.email , roles : user.roles, token: token})
      }catch (err){
        return res.status(404).json({
              message: err.message
          })
      }
    
  }

router.post('/login', login)
module.exports = router
