const express = require('express')
// const { path } = require('../app')
const router = express.Router()
const Document = require('../models/Document')
const SendingDetail = require('../models/SendingDetail')
const A = require('../routes/upload')
const fs = require('fs')
const User = require('../models/User')
const sendingEmail = require('../routes/sendingEmail');


const documents = [
    // {
    //     id: 1,
    //     user_id: '005',
    //     filename: '61160251_RE01',
    //     date: '03/12/2564',
    //     status: 'examine',
    //     type: 'RE01',
    // },
    // {
    //     id: 2,
    //     user_id: '006',
    //     filename: '61160251_RE02',
    //     date: '05/12/2564',
    //     status: 'examine',
    //     type: 'RE02',
    // }
]
let lastId = 3

const getDocuments = async function (req, res, next) {
    try {
        const document = await Document.find({}).sort({updatedAt: 'desc'}).then()
        var documentList = []
        for (i = 0 ; i < document.length; i++){
            var detail = {}
            console.log()
            detail._id = document[i]._id
            detail.userId = document[i].userId
            detail.fileName = document[i].fileName
            detail.path = document[i].path
            detail.status = document[i].status

            const date = new Date(document[i].updatedAt)

            detail.date1 = date.toLocaleDateString('th-TH', {
                year: 'numeric',
                month: 'long',
                day: 'numeric',
            })

            // detail.date1 = document[i].updatedAt

            // detail.name = teacher.name
            documentList.push(detail)
            if (document == null) {
                return res.status(404).json({
                    message: 'Document not found'
                }) 
            }
        }
        res.json(documentList)
    } catch (err) {
        return res.status(500).send({
            message: err.message
        })
    }
}

const getDocument = async function (req, res, next) {
    const id = req.params.id
    var documentList = []
    try {
        const document = await Document.find({userId:id}).sort({updatedAt: 'desc'}).exec()

        for (i = 0 ; i < document.length; i++){
                var detail = {}
                
                detail._id = document[i]._id
                detail.userId = document[i].userId
                detail.fileName = document[i].fileName
                detail.path = document[i].path
                detail.status = document[i].status

                const date = new Date(document[i].updatedAt)

                detail.date1 = date.toLocaleDateString('th-TH', {
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric',
                })

                // detail.date1 = document[i].updatedAt

                if(document[i].status == "กำลังดำเนินการ"){
                    const coupon = await SendingDetail.findOne({documentId:document[i]._id})
                    const teacher = await User.findOne({_id: coupon.teacherId})
                    console.log(teacher)
                    detail.name = teacher.name
                }
                // detail.name = teacher.name
                documentList.push(detail)
                if (document == null) {
                    return res.status(404).json({
                        message: 'Document not found'
                    }) 
                }
            }
            
            res.json(documentList)
    } catch (err) {
        res.status(404).json({
            message: err.message
        })
    }

}

// const getDocumentbyUser_id = function(req,res,next){
//     const index = documents.findIndex(function(item){
//         return item.user_id == req.params.user_id
//     })
//     res.json(documents[index])
// }

const addDocuments = async function (req, res, next) {
    console.log("ผิดปกติในaddDoc")
    if (!req.files) {
        return res.status(400).send("No files were uploaded.");
    }
    const file = req.files.uploadFile
    const path = __dirname + '/files/' + file.name
    const newDocument = new Document({
        userId: req.body.userId,
        fileName: file.name,
        path: path,
        status: "-",
    })
    try {
        file.mv(path, (err) => {
            if (err) {
                return res.status(500).send(err);
            }
        });
        await newDocument.save()
        res.status(201).json(newDocument)
    } catch (err) {
        return res.status(201).send({
            message: err.message
        })
    }
}

const updateDocument = async function (req, res, next) {
    const detail = {}
    const documentId = req.params.id
    const file = req.files.uploadFile
    const path = __dirname + '/files/' + file.name
    const sendingDetailId = await SendingDetail.find({ documentId: documentId }).exec()

    try {
        // const sendingDetail = await SendingDetail.findOne({_id: sendingDetail[i].stuId})
        const document = await Document.findById(documentId)
        const user = await User.findOne({ _id: document.userId })
        // document.userId = req.body.userId
        document.fileName = file.name
        document.path = path
        document.status = req.body.status
        file.mv(path, (err) => {
            if (err) {
                return res.status(500).send(err);
            }
        });
        await document.save()
        detail.email = user.email
        detail.fileName = file.name
        detail.path = path
        detail.note = "เซ็นเอกสารเรียบร้อยเเล้ว"
        sendingEmail.sendingMail(detail)
        await SendingDetail.findOneAndDelete({ documentId: documentId })
        return res.status(200).json(document)
    } catch (err) {
        return res.status(404).send({ message: err.message })
    }
}

const deleteDocument = async function (req, res, next) {
    const documentId = req.params.id
    const sendingDetailId = await SendingDetail.find({ documentId: documentId }).exec()
    const document = await Document.findById(documentId)
    try {
        await fs.unlinkSync(document.path)
        await SendingDetail.findByIdAndDelete(sendingDetailId)
        await Document.findByIdAndDelete(documentId)

        return res.status(200).send("delete")
    } catch (err) {
        return res.status(404).send({ message: err.message })
    }
}



router.get('/', getDocuments)
router.get('/:id', getDocument)
// router.get('/:user_id', getDocumentbyUser_id)
router.post('/', addDocuments)
router.put('/:id', updateDocument)
router.delete('/:id', deleteDocument)

module.exports = router