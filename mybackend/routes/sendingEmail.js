const nodemailer = require("nodemailer");

// async..await is not allowed in global scope, must use a wrapper
async function sendingMail(detail) {
  // Generate test SMTP service account from ethereal.email
  // Only needed if you don't have a real mail account for testing
  let testAccount = await nodemailer.createTestAccount();

  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: 'miuso5555@gmail.com', // generated ethereal user
      pass: 'zuanwmwzmfjcqkvk', // generated ethereal password
    },
  });

  let info = await transporter.sendMail({
    from: '"ระบบส่งเอกสาร" <miuso5555@gmail.com>', // sender address
    to: detail.email, // list of receivers
    subject: detail.note, // Subject line
    text: "", // plain text body
    
    attachments: [
        {
            filename: detail.fileName,
            path: detail.path,
            contentType: 'application/pdf',
        },
    ],
  });

  return console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  //   console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}

module.exports = { sendingMail };

