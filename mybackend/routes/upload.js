const express = require('express')
const path = require('path')
const router = express.Router()
// const fileUpload = require('express-fileupload')

const upload = async function (req, res, next) {
    if (!req.files) {
        return res.status(400).send("No files were uploaded.");
    }

    const file = req.files.uploadFile
    const path = __dirname+'/files/' +file.name
    file.mv(path, (err) => {
        if (err) {
          return res.status(500).send(err);
        }
        return res.send({ status: "success", path: path });
    });
}

router.post('/', upload)
module.exports = router