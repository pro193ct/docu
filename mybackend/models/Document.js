const mongoose = require('mongoose')
const { Schema } = mongoose
const documentSchema = Schema({
    userId: String,
    fileName: String,
    path: String,
    status:String,
    

},
{
    timestamps: true
  }
)

module.exports = mongoose.model('Document',documentSchema)