const { ROLE } = require('../constant.js')
const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const { Schema } = mongoose
const userSchema = Schema({
    email: String,
    password: String,
    roles:{
        type: [String],
        default: [ROLE.STUDENT]
    },
    name: String,
    type: String,
    branch: String,
    studentId: String,
    teacherId:String,

})
userSchema.pre('save', function (next) {
    const user = this
  
    if (this.isModified('password') || this.isNew) {
      bcrypt.genSalt(10, function (saltError, salt) {
        if (saltError) {
          return next(saltError)
        } else {
          bcrypt.hash(user.password, salt, function(hashError, hash) {
            if (hashError) {
              return next(hashError)
            }
  
            user.password = hash
            next()
          })
        }
      })
    } else {
      return next()
    }
  })

module.exports = mongoose.model('User',userSchema)