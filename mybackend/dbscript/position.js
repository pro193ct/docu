const mongoose = require('mongoose')
const Position = require('../models/Position')
mongoose.connect('mongodb://localhost:27017/database')
async function clearPosition(){
    await Position.deleteMany({})
}
async function main (){
    await clearPosition()
    const position = new Position({namePos:'อธิการบดี',userId:'624df30206caffec596445c9'})
    position.save()
    const position1 = new Position({namePos:'รองอธิการบดี',userId:'624df30206caffec596445c8'})
    position1.save()
}

main().then(function(){
    console.log('Finish')
})