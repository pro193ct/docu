const ROLE = {
    STUDENT: 'STUDENT',
    TEACHER: 'TEACHER',
    OFFICER: 'OFFICER',
    ADMIN: 'ADMIN',
}

module.exports = {
  ROLE
}
  
