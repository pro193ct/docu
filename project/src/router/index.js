import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
// import PopupSuccess from '../components/PopupSuccess.vue'
// import Login from '../views/Login.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/popupLoad',
    name: 'PopupLoad',
    component: () =>
      import(/* webpackChunkName: "about" */ '../components/PopupLoad.vue')
  },
  {
    path: '/admin',
    name: 'Admin',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/Admin.vue')
  },
  {
    path: '/tableusers',
    name: 'TableUsers',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/TableUsers.vue')
  },
  {
    path: '/tablepositions',
    name: 'TablePositions',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/TablePositions.vue')
  },
  {
    path: '/teacher',
    name: 'Teacher',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/Teacher.vue')
  },
  {
    path: '/popupDelecte',
    name: 'PopupDelecte',
    component: () =>
      import(/* webpackChunkName: "about" */ '../components/PopupDelecte.vue')
  },
  {
    path: '/popupCancel',
    name: 'PopupCancel',
    component: () =>
      import(/* webpackChunkName: "about" */ '../components/PopupCancel.vue')
  },
  {
    path: '/popupSend',
    name: 'PopupSend',
    component: () =>
      import(/* webpackChunkName: "about" */ '../components/PopupSend.vue')
  },
  {
    path: '/popupCancelTeacher',
    name: 'PopupCancelTeacher',
    component: () =>
      import(/* webpackChunkName: "about" */ '../componentTeacher/PopupCancelTeacher.vue')
  },
  {
    path: '/officer',
    name: 'Officer',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/Officer.vue')
  },
  {
    path: '/student',
    name: 'Student',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/Student.vue')
  },
  {
    path: '/uploadfile',
    name: 'UploadFile',
    component: () =>
      import(/* webpackChunkName: "about" */ '../components/UploadFile.vue')
  },
  {
    path: '/sendnameteacher',
    name: 'SendNameTeacher',
    component: () =>
      import(/* webpackChunkName: "about" */ '../components/SendNameTeacher.vue')
  },
  {
    path: '/sendpositionteacher',
    name: 'SendPositionTeacher',
    component: () =>
      import(/* webpackChunkName: "about" */ '../components/SendPositionTeacher.vue')
  },
  {
    path: '/adduser',
    name: 'AddUser',
    component: () =>
      import(/* webpackChunkName: "about" */ '../componentAdmin/AddUser.vue')
  },
  {
    path: '/addposition',
    name: 'AddPosition',
    component: () =>
      import(/* webpackChunkName: "about" */ '../componentAdmin/AddPosition.vue')
  },
  {
    path: '/edituser',
    name: 'EditUser',
    component: () =>
      import(/* webpackChunkName: "about" */ '../componentAdmin/EditUser.vue')
  },
  {
    path: '/editposition',
    name: 'EditPosition',
    component: () =>
      import(/* webpackChunkName: "about" */ '../componentAdmin/EditPosition.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
